#! /usr/bin/env python
from abc import ABC, abstractproperty
from datetime import datetime
from typing import Dict

from bs4 import BeautifulSoup  # type: ignore

from utils import select_one  # type: ignore


class Listing(ABC):
    def __init__(self, src: BeautifulSoup) -> None:
        self.src = src
        self.retrieved: datetime = datetime.now()  # not a @property because we need it at instantiaion

    def _strings(self, elem: BeautifulSoup) -> str:
        return " ".join([s.strip() for s in elem])

    def as_dict(self) -> Dict[str, str]:
        deets: Dict[str, str] = {}
        for attr in [a for a in dir(self.__class__) if not a.startswith("_")]:
            if attr in ["as_dict"]:
                continue
            if getattr(self, attr):
                deets[attr] = getattr(self, attr)
                deets["retrieved"] = datetime.strftime(self.retrieved, "%d-%m-%Y %H:%M:%S")
        return deets

    def __repr__(self) -> str:

        return "\n".join([f"{k + ':':<20} {v}" for k, v in self.as_dict().items()])

    @abstractproperty
    def id(self) -> str:
        return NotImplemented

    @abstractproperty
    def price(self) -> str:
        return NotImplemented

    @abstractproperty
    def type(self) -> str:
        return NotImplemented

    @abstractproperty
    def name(self) -> str:
        return NotImplemented

    @abstractproperty
    def url(self) -> str:
        return NotImplemented

    @abstractproperty
    def zipcode(self) -> str:
        return NotImplemented

    @abstractproperty
    def city(self) -> str:
        return NotImplemented

    @abstractproperty
    def neighborhood(self) -> str:
        return NotImplemented

    @abstractproperty
    def estate_agent_name(self) -> str:
        return NotImplemented

    @abstractproperty
    def estate_agent_link(self) -> str:
        return NotImplemented

    @abstractproperty
    def surface_area(self) -> str:
        return NotImplemented

    @abstractproperty
    def bedrooms(self) -> str:
        return NotImplemented

    @abstractproperty
    def furniture(self) -> str:
        return NotImplemented

    @abstractproperty
    def availability(self) -> str:
        return NotImplemented

    @abstractproperty
    def location_url(self) -> str:
        return NotImplemented


class ParariusListing(Listing):
    @property
    def id(self) -> str:
        return self.src["data-property-id"]

    @property
    def price(self) -> str:
        a, p, i = [
            s.strip() for s in select_one(self.src, "p.price").strings if s != "\n"
        ]
        return f"{a}{p} {i}"

    @property
    def type(self) -> str:
        return select_one(self.src, "div.details > h2 > a > span.type").string

    @property
    def name(self) -> str:
        return " ".join(
            self._strings(select_one(self.src, "div.details > h2 > a").strings).split()[
                1:
            ]
        )

    @property
    def url(self) -> str:
        return f"https://www.pararius.com{select_one(self.src, 'div.details > h2 > a')['href']}"

    @property
    def zipcode(self) -> str:
        return select_one(self.src, "ul.breadcrumbs > li:nth-child(1)").string.strip()

    @property
    def city(self) -> str:
        return select_one(self.src, "ul.breadcrumbs > li:nth-child(2)").string.strip()

    @property
    def neighborhood(self) -> str:
        return select_one(self.src, "ul.breadcrumbs > li:nth-child(3)").string.strip()

    @property
    def estate_agent_name(self) -> str:
        return select_one(self.src, "p.estate-agent a").string.strip()

    @property
    def estate_agent_link(self) -> str:
        return select_one(self.src, "p.estate-agent a")["href"].strip()

    @property
    def surface_area(self) -> str:
        return select_one(self.src, "ul.property-features > li.surface").string.strip()

    @property
    def bedrooms(self) -> str:
        return select_one(self.src, "ul.property-features > li.bedrooms").string.strip()

    @property
    def furniture(self) -> str:
        return select_one(self.src, "ul.property-features > li.furniture").string.strip()

    @property
    def availability(self) -> str:
        pass

    @property
    def location_url(self) -> str:
        return f"https://www.google.com/maps/place/{self.zipcode.replace(' ', '+')}+{self.city}"

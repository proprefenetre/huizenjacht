#! /usr/bin/env python

from abc import ABC, abstractmethod
from bs4 import BeautifulSoup  # type: ignore
import csv
import json
from pathlib import Path
import requests  # type: ignore
from typing import Any, Callable, Dict, List, Mapping, Sequence, Union

from listing import Listing, ParariusListing  # type: ignore
from utils import select_one  # type: ignore


class Query(ABC):
    possible_filters: List[str] = []

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        for k, v in kwargs.items():
            if k in self.possible_filters:
                setattr(self, k, v)

    def to_csv(self, path: Union[Path, str]) -> None:
        fields = ["id", "price", "type", "name", "url", "zipcode", "city",
                  "neighborhood", "estate_agent_name", "estate_agent_link",
                  "surface_area", "bedrooms", "furniture", "availability",
                  "location_url", "retrieved"]

        if isinstance(path, str):
            path = Path(path)

        with path.open('w', newline='') as f:
            writer = csv.DictWriter(f, fields)
            for l in self.listings():
                writer.writeheader()
                writer.writerow(l.as_dict())

    @abstractmethod
    def _get_next_page(self) -> Union[str, bool]:
        return NotImplemented

    @abstractmethod
    def listings(self, sort=None) -> List[Listing]:
        return NotImplemented


class ParariusQuery(Query):
    possible_filters: List[str] = [
        "location",
        "price_low",
        "price_high",
        "surface",
        "num_rooms",
        "interior",
    ]

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.url = (
            f"https://www.pararius.com/apartments/"
            f"{self.location}/"
            f"{self.price_low}-{self.price_high}/"
            f"{self.surface}m2/"
            f"{self.num_rooms}-bedrooms/"
            f"{self.interior}"
        )

    def _get_next_page(self) -> Union[str, bool]:
        first = BeautifulSoup(requests.get(self.url).text, "lxml")
        try:
            next = select_one(first, "ul.pagination > li.next > a")["href"].split("/").pop()
            if "page" in self.url:
                url = "/".join(self.url.split("/")[:-1])
            else:
                url = self.url
            return f"{url}/{next}"
        except IndexError:
            return False

    def listings(self, sort: Callable[[Listing], str] = None) -> List[Listing]:
        units = []

        while self.url:
            results = BeautifulSoup(requests.get(self.url).text, "lxml").find(
                "ul", class_="search-results-list"
            )
            for element in results.find_all(
                "li", class_="property-list-item-container"
            ):
                units.append(ParariusListing(element))
            self.url = self._get_next_page()  # type: ignore

        if sort:
            units = sorted(units, key=sort)
        return units

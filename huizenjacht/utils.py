#! /usr/bin/env python
from bs4 import BeautifulSoup  # type: ignore


def select_one(bs: BeautifulSoup, selector: str) -> BeautifulSoup:
    return bs.select(selector)[0]


def underline(obj: str, line_char: str = "=") -> str:
    return "{}\n{}".format(obj, len(obj) * line_char)

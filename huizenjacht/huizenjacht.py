#! /usr/bin/env python
from collections import defaultdict
from typing import DefaultDict, List

from utils import underline  # type: ignore
from query import ParariusQuery  # type: ignore


filters = {
    "location": "amsterdam",
    "price_low": "1000",
    "price_high": "1500",
    "surface": 50,
    "num_rooms": 2,
    "interior": "unfurnished",
}

stadsdelen = [f"Stadsdeel {s}" for s in ["Noord", "Oost", "Zuid", "West", "Zuidoost"]]

P = ParariusQuery(**filters)

P.to_csv("ams-1000-1500.csv")
